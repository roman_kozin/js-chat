## NodeJS Chat Core

#### What is there for now?

-   Routes via `HApi` for RESTFul NodeJS
-   Database
    - Schemes
      - User
      - Message
      - Dialogue
      - Attachment
    - Models
      - Users
      - Messages
      - Dialogues
      - Attachments
- Server Runner via `run.js`

#### How to start running?

##### Install dependencies

```bash
cd client && npm i && cd .. && cd server && npm i
```

##### Run the Runner

You can use `pm2`, `nodemon` or `node` to run the chat core

```bash
nodemon run.js &
```