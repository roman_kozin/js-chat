let Mongo = require('mongoose'),
    Schemas = {
        User: require('./Schemes/User.js'),
        Dialogues: require('./Schemes/Dialogue.js'),
        Attachments: require('./Schemes/Attachment.js'),
        Messages: require('./Schemes/Message.js')
    };

module.exports = {
    Users: Mongo.model('User', Schemas.User),
    Dialogues: Mongo.model('Dialogue', Schemas.Dialogues),
    Attachments: Mongo.model('Attachment', Schemas.Attachments),
    Messages: Mongo.model('Messages', Schemas.Messages)
};