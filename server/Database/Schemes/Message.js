let Schema = require('mongoose').Schema,
    Message = new Schema({
        dialogue_id: String,
        from: Number,
        text: String,
        type: {
            type: String,
            enum: [
                'Message',
                'Attachment',
                'Travel'
            ],
            default: 'Message'
        },
        travel: Object
    });

module.exports = Message;