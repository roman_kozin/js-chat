let Schema = require('mongoose').Schema,
    Dialogue = new Schema({
        creator_id: {
            type: Number,
            set: (a) => {
                return parseInt(a);
            }
        },
        users: {
            type: Array,
            set: a => {
                return a.sort();
            }
        },
        unread_notifications: {
            creator: {type: Number, default: 0},
            receiver: {type: Number, default: 0},
        },
        dialogue_type: {
            type: String,
            enum: [
                'Personal',
                'Group'
            ],
            default: 'Personal'
        },
        accepted: {
            creator: {type: Boolean, default: true},
            receiver: {type: Boolean, default: false},
        }
    }, {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    });

module.exports = Dialogue;