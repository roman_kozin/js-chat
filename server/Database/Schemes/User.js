let Schema = require('mongoose').Schema,
    User = new Schema({
        id: {
            type: Number,
            required: true
        },
        first_name: {
            type: String,
            required: true,
            set: (a) => {
                a = a.toString().toLowerCase();
                a[0] = a[0].toUpperCase();
                return a;
            }
        },
        last_name: {
            type: String,
            required: true,
            set: (a) => {
                a = a.toString().toLowerCase();
                a[0] = a[0].toUpperCase();
                return a;
            }
        },
        avatar: {
            type: String,
            required: false,
            default: 'https://placehold.it/100x100'
        },
        online: {
            type: Boolean,
            default: true
        },
        token: {
            type: String,
            required: true
        }
    }, {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    });

/**
 * Virtual Fields : Full Name
 */
User
    .virtual('full_name')
    .get(() => {
        return `${this.first_name} ${this.last_name}`;
    });

module.exports = User;