let Models = require('./Database/Models.js'),
    Logger = require('winston'),
    MongoDB = require('mongoose'),
    SocketHttpServer = require('http').createServer(),
    SocketHttpServerPort = 2202,
    ApiServer = new (require('hapi')).Server(),
    Routes = require('./Routes.js'),
    SocketIOHost = require('socket.io')(SocketHttpServer);

ApiServer.connection({port: 2101});
ApiServer.route(Routes);

ApiServer.start(() => {

    Logger.info('Setting up MongoDB connection');

    MongoDB.Promise = Promise;

    MongoDB.connect('mongodb://localhost:27017/lastVibeChat', {
        useMongoClient: true
    });

    let _f = {
        send: (data, errors, message, code) => {
            return {
                data: data ? data : null,
                errors: errors ? errors : null,
                message: message ? message : 'OK',
                code: code ? code : 200
            }
        },
        Dialogues: {
            create: (users, creator) => {

                return Models
                    .Dialogues
                    .findOne()
                    .where('users').equals(users.sort())
                    .then((a) => {
                        if (!a) {
                            return Models.Dialogues.create({users: users, creator_id: creator})
                                .then((c) => {

                                    if (c) {
                                        Logger.info('Dialogue has been created');

                                        return c;
                                    }

                                    return false;
                                });
                        }

                        Logger.info(`Chat exists with ID of ${a.id}`);
                        return a;
                    });

            },
            getDialogues: (Socket, user_id, limit, page) => {

                limit = limit ? limit : 10;
                page = page ? (page > 0 ? page - 1 : 0) : 0;

                let _results = Models.Dialogues
                    .find({
                        users: {$in: [user_id]}
                    })
                    .skip(limit * page)
                    .limit(limit)
                    .sort({'updated_at': 1});

                _results
                    .then((a) => {
                        Socket.emit(`Dialogues/set/${user_id}`,
                            _f.send({
                                Dialogues: a.map((b) => {
                                })
                            })
                        );
                    });

            },
            acceptInvitation: (dialogue_id, user_id) => {

                let _dialogue = Models.Dialogues
                    .findOne({
                        users: {$contains: user_id}
                    });

                return _dialogue
                    .exec((error, dialogue) => {

                        if (error) {
                            return error;
                        }

                        if (dialogue.creator !== user_id) {
                            if (dialogue.accepted.receiver === false) {
                                dialogue.accepted.receiver = true;

                                return dialogue.save((error, dialogue) => {
                                    if (error) return error;

                                    return {action: true, dialogue: dialogue};

                                });
                            }

                            return {action: false, dialogue: dialogue};
                        }
                    });
            }
        },
        Messages: {
            send: (dialogue_id, sender, type, body) => {

            }
        },
        Users: {
            updateOrCreate: (a) => {

                Models
                    .Users
                    .findOneAndUpdate(
                        {id: a.id},
                        a,
                        {upsert: true}
                    )
                    .then((a) => {
                        Logger.info(`${a.first_name} ${a.last_name} has been checked`);
                    });
            }
        }
    };

    Logger.info('Trying to Set-Up socket connection');
    SocketIOHost.on('connection', (SocketClient) => {
        Logger.info(`Connected with Socket ID ${SocketClient.id}`);

        SocketClient.on('send', (a) => {
        });

        SocketClient.on('Dialogues/get/req', (a) => {
            Logger.info(`Sending Dialogues list to UserID ${a.user.id}`);
            _f.Dialogues.getDialogues(SocketClient, a.user.id, a.limit, a.page);
        });

        SocketClient.on('Dialogues/create', (a) => {
            Logger.info('Creating Dialogue with users [' + a.users.join(' and ') + ']');
            _f.Dialogues.create(a.users, a.creator);

            for (let i = 0; i < a.users.length; i++) {
                SocketClient.emit(`Dialogues/set/${a.users[i]}`, _f.send(
                    {Dialogues: _f.Dialogues.getDialogues(a.users[i])}
                ));
            }

        });

        SocketClient.on('user', (a) => {
            _f.Users.updateOrCreate(a);
        });

        SocketClient.emit('connected', _f.send({connected: true}));
    });

    SocketHttpServer.listen(SocketHttpServerPort);

    Logger.info('Houston, we have been started..psshhht...');
});